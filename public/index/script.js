document.addEventListener('DOMContentLoaded', () => {
    const abbreviations = {
        "some": "Soma: A Latin word meaning 'body'. Used by biologists to describe various entities.",
        "omic": "\"Omic\"s refer to a broad discipline of science and biology that focuses on the comprehensive " +
            "study and analysis of various biological systems. The term \"omics\" is used as a suffix to denote the " +
            "study of related sets of molecules or processes within the field of biology.",
        "ome": "According to ChatGPT, \"ome\" is a suffix used in biology to denote the totality or complete set of " +
            "a particular type of molecular entity or system within an organism or environment, such as " +
            "genes in a genome or proteins in a proteome."
    };

    [...document.getElementsByTagName('abbr')].forEach(e => {
        if (!e.title && e.innerText.toLowerCase() in abbreviations) {
            e.title = abbreviations[e.innerText.toLowerCase()]
        }
    })
})

window.addEventListener('mousemove', event => {
    document.querySelectorAll('.cards > div, .cards article').forEach(element => {
        const rect = element.getBoundingClientRect()
        const x = event.clientX - rect.left
        const y = event.clientY - rect.top
        element.classList.add('has-mouse-pos')
        element.style.setProperty('--mouse-x', x + 'px')
        element.style.setProperty('--mouse-y', y + 'px')
    })
})

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.cards > div').forEach(element => {
        element.querySelectorAll('article a').forEach(button => {
            button.addEventListener('mouseenter', () => {
                element.classList.add('clicking')
            })
            button.addEventListener('mouseleave', () => {
                element.classList.remove('clicking')
            })
        });
    })

    document.querySelectorAll("article[new-until]").forEach(article => {
        const newUntil = new Date(article.getAttribute('new-until'))
        if (newUntil > new Date()) {
            const header = article.querySelector(':scope > :is(h1, h2, h3, h4, h5, h6)')
            const sup = document.createElement('sup')
            sup.innerText = "NEW!"
            sup.className = "new-indicator"
            header.appendChild(sup)
        }
    })
})
