const fu = location.hash === "#fu"

const domContentLoadedPromise = new Promise(resolve => document.addEventListener('DOMContentLoaded', resolve))

const scatter = {
    x: [],
    y: [],
    mode: 'markers',
    text: [],
    type: 'scatter',
    color: [],

    // custom
    pmid: [],
    authors: [],
}
const traces = [scatter]
const layout = {
    xaxis: {
        type: 'date',
        title: "First mentioned",
    },
    yaxis: {
        type: fu || 'log',
        title: fu ? "Name length" : "Mentioned in articles",
    },
}
const plotPromise = domContentLoadedPromise
    .then(element => Plotly.newPlot('chart', traces, layout))
    .then(plot => window.plot = plot)

const dataPromise = fetch("https://gitlab.com/api/v4/projects/50664866/repository/files/somes.json/raw")
    .then(r => r.json())
    .then(data => window.data = data)

Promise.all([plotPromise, dataPromise]).then(([plot, data]) => {
    scatter.x = []
    scatter.y = []
    scatter.text = []
    scatter.pmid = []
    scatter.authors = []
    if (fu) {
        scatter.marker = scatter.marker || {}
        scatter.marker.size = []
    }
    for (const [soma, {mentions, pmid, issued, authors}] of Object.entries(data)) {
        scatter.x.push(issued)
        if (fu) {
            scatter.y.push(soma.length)
            scatter.marker.size.push(Math.log(mentions) * 2 + 3)
        } else {
            scatter.y.push(mentions)
        }
        scatter.text.push(soma)
        scatter.pmid.push(pmid)
        scatter.authors.push(authors)
    }

    Plotly.redraw('chart')
})

domContentLoadedPromise.then(() => document.getElementById('chart').on('plotly_click', (clickData) => {
    const {pointNumber, data} = clickData.points[0]
    const pmid = data.pmid[pointNumber]
    window.open(`https://pubmed.ncbi.nlm.nih.gov/${pmid}/`, '_blank')
}))

function filterChart() {
    const filterString = document.getElementById('search').value

    if (filterString) {
        scatter.selectedpoints = []
        for (const i in scatter.text) {
            if (scatter.text[i].match(filterString)) {
                scatter.selectedpoints.push(i)
            }
        }
    } else {
        scatter.selectedpoints = undefined
    }

    clearTimeout(filterChart.redrawTimeout)
    filterChart.redrawTimeout = setTimeout(() => {
        Plotly.redraw('chart')
    }, 0)
}
