async function loadTimeline() {
    try {
        const url = "https://gitlab.com/api/v4/projects/50664866/repository/files/somes.json/raw"
        const response = await fetch(url);
        const data = await response.json();
        createTimeline(data);
    } catch (error) {
        console.error('Error loading JSON:', error);
    }
}

function createTimeline(data) {
    const timeline = document.getElementById('timeline');
    const sortedData = sortDataByYear(data);
    for (let year = 1975; year <= new Date().getFullYear(); year++) {
        const entries = sortedData[year] || [];
        entries.sort((a, b) => new Date(a.issued).getTime() - new Date(b.issued).getTime())
        const yearDiv = document.createElement('div');
        yearDiv.className = 'year';
        yearDiv.innerHTML = `
<div class="year-marker"></div>
<div class="year-number">${year}</div>
`
        entries.forEach(entry => {
            const entryA = document.createElement('a');
            entryA.className = 'entry';
            entryA.href = `https://pubmed.ncbi.nlm.nih.gov/${entry.pmid}/`
            entryA.target = '_blank'
            entryA.innerHTML = `
<div class="entry-name">
    <span class="the-entry-name">${entry.name}</span>
    (${new Date(entry.issued).toLocaleDateString()})
</div>
<div class="entry-authors">
    <span class="the-entry-authors">${entry.authors.join(', ')}</span>
</div>
`;
            entryA.style.opacity = 0
            yearDiv.appendChild(entryA);
            setTimeout(() => entryA.style.opacity = 1, 0)
        });
        timeline.appendChild(yearDiv);
    }
}

function sortDataByYear(data) {
    const sortedData = {};
    for (const [name, entry] of Object.entries(data)) {
        const year = new Date(entry.issued).getFullYear();
        if (!sortedData[year]) sortedData[year] = [];
        sortedData[year].push({...entry, name});
    }
    return sortedData;
}

function filterTimeline() {
    const search = document.getElementById('search').value.toLowerCase();

    let closest = undefined;
    const scrollOffsetWidth = document.getElementById('timeline-container').ow
    const scrollLeft = document.getElementById('timeline-container').scrollLeft

    document.querySelectorAll('.entry').forEach(entry => {
        const text = entry.textContent.toLowerCase();
        const matches = text.includes(search);
        entry.style.opacity = matches ? '1' : '0.3';
        // entry.querySelector('.entry-authors').style.maxHeight = matches ? 'initial' : '0';

        if (matches) {
            if (closest === undefined) closest = entry
            else {
                const closestYear = closest.parentElement
                const entryYear = entry.parentElement
                const oldDiff = Math.abs((closestYear.ol + closestYear.ow / 2) - (scrollLeft + scrollOffsetWidth / 2));
                const newDiff = Math.abs((entryYear.ol + entryYear.ow / 2) - (scrollLeft + scrollOffsetWidth / 2));
                if (oldDiff > newDiff) {
                    closest = entry;
                }
            }
        }
    });

    window.closest = closest

    if (closest) {
        const closestYear = closest.parentElement

        // todo check if requestAnimationFrame would be better
        clearTimeout(filterTimeline.hScrollTimeout)
        filterTimeline.hScrollTimeout = setTimeout(() => {
            // todo don't rely on gc, use rather just a global variable
            const scrollLeft = document.getElementById('timeline-container').scrollLeft
            const scrollRight = scrollLeft + scrollOffsetWidth
            const closestLeft = closestYear.ol;
            const closestMiddle = closestLeft + closestYear.ow / 2
            const closestRight = closestLeft + closestYear.ow
            if (closestMiddle < scrollLeft || closestMiddle > scrollRight) {
                const scrollTarget = closestMiddle < scrollLeft ? closestLeft : closestRight - scrollOffsetWidth
                document.getElementById('timeline-container').scrollTo({left: scrollTarget, behavior: "smooth"})
            }
        }, 0)

        // todo check if requestAnimationFrame would be better
        clearTimeout(filterTimeline.vScrollTimeout)
        filterTimeline.vScrollTimeout = setTimeout(() => {
            // todo don't rely on gc, use rather just a global variable
            if (closest.offsetTop + closest.offsetHeight < closestYear.scrollTop || closest.offsetTop > closestYear.scrollTop + closestYear.offsetHeight) {
                closestYear.scrollTo({
                    top: (closest.offsetTop + closest.offsetHeight) / 2 + closestYear.offsetHeight / 2,
                    behavior: "smooth"
                })
            }
        }, 0)
    }
}

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('timeline-container').addEventListener('wheel', function (e) {
        if (!e.altKey) {
            // noinspection JSSuspiciousNameCombination
            this.scrollLeft += e.deltaY;  // Corrected to scroll timeline
            e.preventDefault()
        }
    });

    loadTimeline().finally(() => {
        document.querySelector('.loader').style.display = 'none';
        // document.querySelector('#app').style.display = undefined;

        document.querySelectorAll('.entry').forEach(e => {
            // caching offset calculation and also shorthand
            e.ol = e.offsetLeft
            e.ow = e.offsetWidth
            e.or = e.ol + e.ow
        })

        document.querySelectorAll('.year').forEach(e => {
            // caching offset calculation and also shorthand
            e.ol = e.offsetLeft
            e.ow = e.offsetWidth
            e.or = e.ol + e.ow
        })

        document.querySelectorAll('#timeline-container').forEach(e => {
            e.ow = e.offsetWidth;
        })
    });
})
